

class ApiResponse {
    dynamic response;

    ApiResponse({this.response});

    factory ApiResponse.fromJson(Map<String, dynamic> json) {
        return ApiResponse(
            response: json['response'],
        );
    }
    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = <String, dynamic>{};

        if (response != null) {
            data['response'] = response.map((v) => v.toJson()).toList();
        }
        return data;
    }

    @override
    String toString() {
        return 'ApiResponse{ response: $response}';
    }
}