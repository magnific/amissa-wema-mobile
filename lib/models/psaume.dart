class  Psaume {
  String ref;
  String repond;
  String texte;

  Psaume({required this.ref, required this.repond, required this.texte});

  factory Psaume.fromJson(Map<String, dynamic> json) {
    return Psaume(
      ref : json['ref'],
      repond : json['repond'],
      texte : json['texte'],
    );
  }

  @override
  String toString() {
    return 'Ticket { reference : $ref, response: $repond }';
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['ref'] = ref;
    map['repond'] = repond;
    map['texte'] = texte;
    return map;
  }
}
