import 'lecture_1.dart';
import 'psaume.dart';
import 'lecture_2.dart';
import 'evangile.dart';

class Liturgie {

   Lecture1 lecture1;
   Psaume? psaume;
   Lecture2? lecture2;
   Evangile? evangile;

  Liturgie({required this.lecture1, this.psaume,  this.lecture2,  this.evangile});

  factory Liturgie.fromJson(Map<String, dynamic> json) {
    return Liturgie(
      lecture1: Lecture1.fromJson(json["lecture_1"]),
      psaume: Psaume.fromJson(json["psaume"]),
      lecture2: Lecture2.fromJson(json["lecture_2"]),
      evangile: Evangile.fromJson(json["evangile"]),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "lecture1": lecture1,
      "psaume": psaume,
      "lecture2": lecture2,
      "evangile": evangile,
    };
  }

  @override
  String toString() {
    return 'Liturgie{lecture1: $lecture1, psaume: $psaume, lecture2: $lecture2, evangile: $evangile}';
  }
//

}