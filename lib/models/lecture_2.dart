class Lecture2 {
  String ref;
  String text;

  Lecture2({required this.ref, required this.text});

  factory Lecture2.fromJson(Map<String, dynamic> json) {
    return Lecture2(
      ref: json["ref"],
      text: json["text"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "ref": ref,
      "text": text,
    };
  }

  @override
  String toString() {
    return 'Lecture2{ref: $ref, text: $text}';
  }
//
}