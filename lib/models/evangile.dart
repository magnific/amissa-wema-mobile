class Evangile {

  Evangile({
      required this.antienne,
      required this.refe,
      required this.text,});

  String antienne;
  String refe;
  String text;

  factory Evangile.fromJson(Map<String, dynamic> json) {
    return Evangile(
      antienne: json["antienne"],
      refe: json["refe"],
      text: json["text"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "antienne": antienne,
      "refe": refe,
      "text": text,
    };
  }

  @override
  String toString() {
    return 'Evangile{antienne: $antienne, refe: $refe, text: $text}';
  }
//


}