class Lecture1 {

  String ref;
  String text;

  Lecture1({
      required this.ref,
      required this.text,});

  factory Lecture1.fromJson(Map<String, dynamic> json) {
    return Lecture1(
      ref: json["ref"],
      text: json["text"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "ref": ref,
      "text": text,
    };
  }

  @override
  String toString() {
    return 'Lecture1{ref: $ref, text: $text}';
  }
//



}