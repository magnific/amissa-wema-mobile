import 'package:flutter/material.dart';
import 'package:wema/models/lecture_1.dart';

class FirstReading extends StatefulWidget {

  late Lecture1 lecture_1;

   FirstReading({Key? key, required this.lecture_1}) : super(key: key);


  @override
  State<FirstReading> createState() => _FirstReadingState();
}

class _FirstReadingState extends State<FirstReading> {
  @override
  Widget build(BuildContext context) {
    return  Center(
      child:  Container(
        padding: const EdgeInsets.all(10.0),
        alignment: Alignment.center,
        child: Column(
          children:  <Widget> [
            Expanded(
              child: Text( widget.lecture_1.ref.toString(),
              style: const TextStyle(decoration: TextDecoration.none,
              fontSize: 14.0),),
            ),
            Expanded(child: Text(widget.lecture_1.text,
              style: const TextStyle(decoration: TextDecoration.none,
                  fontSize: 14.0),)
            ),
          ],
        ),
      ),

    );
  }
}
