import 'package:flutter/material.dart';
import 'package:wema/controllers/liturgies_controller.dart';
import 'package:wema/models/lecture_1.dart';
import 'package:wema/models/liturgie.dart';
import 'package:wema/pages/first_reading_screen.dart';
import 'package:provider/provider.dart';
import '../locators.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    locator<LiturgiessController>().getLiturgie('2023-01-10');
  }

  @override
  Widget build(BuildContext context) {
    late Liturgie liturgie = Provider.of<LiturgiessController>(context).liturgie;
    bool isLoading = Provider.of<LiturgiessController>(context).isLoading;

    return DefaultTabController(
        initialIndex: 0,
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            // Here we take the value from the MyHomePage object that was created by
            // the App.build method, and use it to set our appbar title.
            backgroundColor: Colors.redAccent,
            iconTheme: const IconThemeData(
              color: Colors.white,
            ),
            title: const Text("Amissa"),
            actions: [
              IconButton(
                icon: const Icon(Icons.calendar_month),
                onPressed: () {},
              ),
              IconButton(
                icon: const Icon(Icons.share),
                onPressed: () {},
              ),
              IconButton(
                icon: const Icon(Icons.more),
                onPressed: () {},
              ),
            ],
            bottom: const TabBar(
              indicatorColor: Color(0xff4961F6),
              tabs: <Widget>[
                Tab(
                  text: "Nuxixa nukɔntɔn ɔ́",
                ),
                Tab(
                  text: "Ɖɛhan ɔ́",
                ),
                Tab(
                  text: "WƐNƉAGBE Ɔ́",
                ),
              ],
            ),
          ),
          drawer: Drawer(
            child: Container(
              color: Colors.white,
            ),
          ),
          body: (isLoading)
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              :  TabBarView(
                  children: <Widget>[
                    FirstReading(lecture_1: liturgie.lecture1),
                    Text("Psaume "),
                    Text("Evangile     ....")
                  ],
                ),
        ));
  }
}
