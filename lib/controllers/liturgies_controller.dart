import 'dart:async';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wema/models/liturgie.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';

class LiturgiessController extends ChangeNotifier {
  String userUrl = 'https://amissazema-shounsa.koyeb.app/liturgie/2023-01-10';
  bool _isLoading = false;

  bool get isLoading => _isLoading;

  late Liturgie liturgie;

  Future<Liturgie?> getLiturgie(String? theDate) async {
    _isLoading = true;
    notifyListeners();

    final result = await http.get(Uri.parse(userUrl)).catchError((e) {
      print('Error Fetching Users');
    });

    if (result.statusCode == 201) {
      Map<String, dynamic> data = json.decode(result.body);
      var _data = data["data"]['liturgie'];

      if (_data != null) {
        liturgie = Liturgie.fromJson(_data);
      }

      _isLoading = false;
      notifyListeners();
      return liturgie;
    } else {
      _isLoading = false;
      notifyListeners();
      throw Exception('Error - ${result.statusCode}');
    }
  }
}
